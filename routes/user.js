const express = require('express')
const router = express.Router()
const User = require('../model/User')

const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).exec()
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getUser = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(user)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addUsers = async function (req, res, next) {
  // const newUser = {
  //   id: lastId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // users.push(newUser)
  // lastId++
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateUser = async function (req, res, next) {
  const userId = (req.params.id)
  try {
    const user = await User.findById(userId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
  // const user = {
  //   id: parseInt(req.body.id),
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = users.findIndex(function (item) {
  //   return item.id === userId
  // })
  // if (index >= 0) {
  //   users[index] = user
  //   res.json(users[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No user id ' + req.params.id

  //   })
  // }
}

const deleteUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
  // const index = users.findIndex(function (item) {
  //   return item.id === userId
  // })
  // if (index >= 0) {
  //   users.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No user id ' + req.params.id

  //   })
  // }
}
router.get('/', getUsers)
router.get('/:id', getUser)
router.post('/', addUsers)
router.put('/:id', updateUser)
router.delete('/:id', deleteUser)

module.exports = router
