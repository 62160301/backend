const express = require('express')
const router = express.Router()
const Product = require('../model/Product')

const getProdcuts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getProdcut = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found!!'
      })
    }
    res.json(product)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addProdcuts = async function (req, res, next) {
  // const newProduct = {
  //   id: lastId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // products.push(newProduct)
  // lastId++
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateProduct = async function (req, res, next) {
  const productId = (req.params.id)
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
  // const product = {
  //   id: parseInt(req.body.id),
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   products[index] = product
  //   res.json(products[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No product id ' + req.params.id

  //   })
  // }
}

const deleteProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   products.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No product id ' + req.params.id

  //   })
  // }
}
router.get('/', getProdcuts)
router.get('/:id', getProdcut)
router.post('/', addProdcuts)
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)

module.exports = router
